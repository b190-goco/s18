console.log("Hello World!");
/*
	MINIACTIVITY
	
	create a function
		create a let variable, value prompt();
		log
	invoke
*/

/*function babilina(){
	let sandwidge = prompt("Is that my camera? (Yes/No): ");
	console.log("Your answer is "+sandwidge);
}

babilina();*/
// may not be ideal since sandwidge is local

function printName(name) {
	// name - parameter (not initialized but still works)
	console.log("Hello, "+name);
}

//printName("Hello Hello");
printName("Joana");
printName("John");
printName("Jane");

let sampleVar = "Yua";
printName(sampleVar);

/*
	MINIACTIVITY
		create 2 function
			1st with 2 para
				log "the numberspassed as arguments are:"
				log para1
				log para2
			invoke

			2nd with 3 para
				log "My friends are para1, para2, para3."
			invoke

*/

function get2Numbers(num1,num2){
	console.log("the numbers passed as arguments are:");
	console.log(num1);
	console.log(num2);
}
get2Numbers(11,15);
function myFriends(fr1,fr2,fr3) {
	console.log("My friends are "+fr1+" "+fr2+" "+fr3+" ");

}
myFriends("Chandler","Monica","Phoebe");

// improving use of parameters
function checkDivisibilityBy8(num){
	let remainder = num%8;
	console.log("The remainder of "+num+"is "+remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is "+num+" divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

/*
	MINIACTIVITY
	isDivisibleBy4
*/

function checkDivisibilityBy4(num){
	let remainder = num%4;
	console.log("The remainder of "+num+"is "+remainder);
	let isDivisibleBy4 = remainder === 0;
	console.log("Is "+num+" divisible by 4?");
	console.log(isDivisibleBy4);
}

checkDivisibilityBy4(64);
checkDivisibilityBy4(28);

/*let numEven =*/ function isEven(num){
	console.log("Is "+num+" even?");
	let ans = num % 2 == 0;
	console.log(ans);
}

/*let numOdd =*/ function isOdd(num1){
	console.log("Is "+num1+" odd?");
	let ans = num1 %2 !== 0;
	console.log(ans);
}

isEven(20);
isOdd(31);

/*console.log(isEven);
console.log(isOdd);*/

// FUNCTION AS ARGUMENTS
// function paramtes can accept other functions as arguments
// used for complex processes
function argFunc() {
	console.log("funct pass to another func");
}

function invFunc(funcPara) {
	funcPara();
}

invFunc(argFunc);
// wont work if both have parenthesis

let firstName = "Juan", middleName = "Dela", lastName = "Cruz";
console.log(firstName+" "+middleName+" "+lastName);

function returnFullName(firstName, middleName, lastName) {
	return firstName +" "+middleName+" "+lastName;
	//console.log("This message will not be printed.");
	// wont show cuz code ended in 'return'
}
//returnFullName("Jeffrey","Smith","Bezos");
alert(returnFullName("Jeffrey","Smith","Bezos"));

let completeName = returnFullName("Jeffrey","Smith","Bezos");
console.log(completeName);

/*
	MINIACTIVITY

	make use of 2 para for full address
		return value of 2 arg
	log
*/

function returnAddress(city,prov) {
	return city +" "+ prov;
}
console.log("Full Address:");
console.log(returnAddress("Valenzuela","Metro Manila"));

function printPlayerInfo(username,level,job) {
	console.log("Username: "+username);
	console.log("Level: "+level);
	console.log("Job Class: "+job);
	return [username,level,job];
}
let user1 = printPlayerInfo("chocoboknight",100,"Black Mage");
console.log(user1);
